const express = require('express');
const router = express.Router();
const app = express();
var server = require('http').createServer(app);
var port = process.env.PORT || 8080;

  app.get('/menu', (req, res) => {
    res.render('menu');
  });

  app.get('/game', function(req,res){
      res.render('game');
  })

  app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.use(express.static(__dirname + '/'));

var server = app.listen(port, function () {
    var adressHost = server.address().address;
    var portHost = server.address().port;
    console.log('Ecoute à l\'adresse http://%s:%s', adressHost, portHost);
  });